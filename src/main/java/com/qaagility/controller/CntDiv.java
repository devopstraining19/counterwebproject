package com.qaagility.controller;

public class CntDiv {

    public int division(int firstVal, int secondVal) {
        if (secondVal == 0) {
            return Integer.MAX_VALUE;
	}	
        else {
            return firstVal / secondVal;
	}
    }

}
