package com.qaagility.controller;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class CntDivTest {

    @Test
    public void testZero() throws Exception {

        CntDiv calc = new CntDiv();
        assertEquals("Verify add()",Integer.MAX_VALUE,calc.division(0,0));
    }

    @Test
    public void testDiv() throws Exception {

        CntDiv calc = new CntDiv();
        assertEquals("Verify add()",5,calc.division(10,2));
    }

}
